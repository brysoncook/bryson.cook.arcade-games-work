""" Sprite Sample Program """

import arcade

# --- Constants ---
SPRITE_SCALING_BOX = 0.47
SPRITE_SCALING_PLAYER = 0.38

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

MOVEMENT_SPEED = 5


class MyWindow(arcade.Window):
    """ Our custom Window Class"""

    def __init__(self):
        """ Initializer """
        # Call the parent class initializer
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "Sprite Example")

        # Sprite lists
        self.all_sprites_list = None
        self.wall_list = None
        self.gem_list = None

        # Set up the player
        self.player_sprite = None

        # This variable holds our simple "physics engine"
        self.physics_engine = None

        self.chimes_sound = arcade.load_sound("images/chimes.ogg")

    def setup(self):

        # Set the background color
        arcade.set_background_color(arcade.color.AMAZON)

        # Sprite lists
        self.all_sprites_list = arcade.SpriteList()
        self.wall_list = arcade.SpriteList()
        self.gem_list = arcade.SpriteList()

        # Reset the score
        self.score = 0

        gem = arcade.Sprite("images/gemBlue.png", SPRITE_SCALING_BOX)
        gem.center_x = 702
        gem.center_y = 340
        self.gem_list.append(gem)
        self.all_sprites_list.append(gem)

        gem = arcade.Sprite("images/gemBlue.png", SPRITE_SCALING_BOX)
        gem.center_x = 702
        gem.center_y = 230
        self.gem_list.append(gem)
        self.all_sprites_list.append(gem)

        gem = arcade.Sprite("images/gemBlue.png", SPRITE_SCALING_BOX)
        gem.center_x = 90
        gem.center_y = 335
        self.gem_list.append(gem)
        self.all_sprites_list.append(gem)

        gem = arcade.Sprite("images/gemBlue.png", SPRITE_SCALING_BOX)
        gem.center_x = 580
        gem.center_y = 210
        self.gem_list.append(gem)
        self.all_sprites_list.append(gem)


        # Create the player
        self.player_sprite = arcade.Sprite("images/alienBlue_walk1.png", SPRITE_SCALING_PLAYER)
        self.player_sprite.center_x = 100
        self.player_sprite.center_y = 100
        self.all_sprites_list.append(self.player_sprite)

        # --- Manually place walls

        wall = arcade.Sprite("images/spikes.png", SPRITE_SCALING_BOX)
        wall.center_x = 702
        wall.center_y = 300
        self.wall_list.append(wall)
        self.all_sprites_list.append(wall)

        wall = arcade.Sprite("images/spikes.png", SPRITE_SCALING_BOX)
        wall.center_x = 580
        wall.center_y = 165
        self.wall_list.append(wall)
        self.all_sprites_list.append(wall)

        wall = arcade.Sprite("images/spikes.png", SPRITE_SCALING_BOX)
        wall.center_x = 90
        wall.center_y = 285
        self.wall_list.append(wall)
        self.all_sprites_list.append(wall)

        # --- Place boxes inside a loop
        for x in range(30, 800, 61):
            wall = arcade.Sprite("images/brickBrown.png", SPRITE_SCALING_BOX)
            wall.center_x = x
            wall.center_y = 30
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        for x in range(30, 800, 61):
            wall = arcade.Sprite("images/brickBrown.png", SPRITE_SCALING_BOX)
            wall.center_x = x
            wall.center_y = 570
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        for y in range(32, 600, 61):
            wall = arcade.Sprite("images/brickBrown.png", SPRITE_SCALING_BOX)
            wall.center_x = 30
            wall.center_y = y
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        for y in range(32, 600, 61):
            wall = arcade.Sprite("images/brickBrown.png", SPRITE_SCALING_BOX)
            wall.center_x = 765
            wall.center_y = y
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        for x in range(150, 440, 61):
            wall = arcade.Sprite("images/rock.png", SPRITE_SCALING_BOX)
            wall.center_x = x
            wall.center_y = 150
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        for x in range(150, 440, 61):
            wall = arcade.Sprite("images/fence.png", SPRITE_SCALING_BOX)
            wall.center_x = x
            wall.center_y = 458
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        for y in range(163, 300, 61):
            wall = arcade.Sprite("images/brickBrown.png", SPRITE_SCALING_BOX)
            wall.center_x = 519
            wall.center_y = y
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        for y in range(163, 420, 61):
            wall = arcade.Sprite("images/brickBrown.png", SPRITE_SCALING_BOX)
            wall.center_x = 641
            wall.center_y = y
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        for y in range(285, 420, 61):
            wall = arcade.Sprite("images/brickBrown.png", SPRITE_SCALING_BOX)
            wall.center_x = 151
            wall.center_y = y
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        for y in range(224, 350, 61):
            wall = arcade.Sprite("images/brickBrown.png", SPRITE_SCALING_BOX)
            wall.center_x = 273
            wall.center_y = y
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        for y in range(285, 420, 61):
            wall = arcade.Sprite("images/brickBrown.png", SPRITE_SCALING_BOX)
            wall.center_x = 395
            wall.center_y = y
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        for y in range(448, 540, 61):
            wall = arcade.Sprite("images/brickBrown.png", SPRITE_SCALING_BOX)
            wall.center_x = 519
            wall.center_y = y
            self.wall_list.append(wall)
            self.all_sprites_list.append(wall)

        # Create the physics engine. Give it a reference to the player, and
        # the walls we can't run into.
        self.physics_engine = arcade.PhysicsEngineSimple(self.player_sprite, self.wall_list)

    def on_draw(self):
        arcade.start_render()
        self.all_sprites_list.draw()

        # Put the text on the screen.
        output = f"Score: {self.score}"
        arcade.draw_text(output, 10, 20, arcade.color.WHITE, 14)

    def update(self, delta_time):
        self.physics_engine.update()

        hit_list = arcade.check_for_collision_with_list(self.player_sprite,
                                                        self.gem_list)

        for gem in hit_list:
            arcade.play_sound(self.chimes_sound)
            gem.kill()
            self.score += 1

    def on_key_press(self, key, modifiers):
        """Called whenever a key is pressed. """

        if key == arcade.key.UP:
            self.player_sprite.change_y = MOVEMENT_SPEED
        elif key == arcade.key.DOWN:
            self.player_sprite.change_y = -MOVEMENT_SPEED
        elif key == arcade.key.LEFT:
            self.player_sprite.change_x = -MOVEMENT_SPEED
        elif key == arcade.key.RIGHT:
            self.player_sprite.change_x = MOVEMENT_SPEED

    def on_key_release(self, key, modifiers):
        """Called when the user releases a key. """

        if key == arcade.key.UP or key == arcade.key.DOWN:
            self.player_sprite.change_y = 0
        elif key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.player_sprite.change_x = 0


def main():
    window = MyWindow()
    window.setup()
    arcade.run()


main()