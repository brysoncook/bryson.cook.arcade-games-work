def main():

    room_list = []

    room = ["You are in the library, you may travel North or East.", 3, 1, None, None]
    room_list.append(room)
    room = ["You are in the south hallway, you may travel North, East, or West.", 4, 2, None, 0]
    room_list.append(room)
    room = ["You are in the games room, you may travel North or West.", 5, None, None, 1]
    room_list.append(room)
    room = ["You are in the dinning room, you may travel East or South.", None, 4, 0, None]
    room_list.append(room)
    room = ["You are in the north hallway, you may travel East, South, or West.", None, 5, 1, 3]
    room_list.append(room)
    room = ["You are in the kitchen, you may travel South or West.", None, None, 2, 4]
    room_list.append(room)

    current_room = 0
    done = False

    while not done:
        print()
        print(room_list[current_room][0])
        user_input = input("Which direction do you want to go: ")
        if user_input.lower() == "north" or user_input.lower() == "n":
            next_room = room_list[current_room][1]
            if next_room is None:
                print("You can't go that way")
            else:
                current_room = next_room

        elif user_input.lower() == "east" or user_input.lower() == "e":
            next_room = room_list[current_room][2]
            if next_room is None:
                print("You can't go that way")
            else:
                current_room = next_room

        elif user_input.lower() == "south" or user_input.lower() == "s":
            next_room = room_list[current_room][3]
            if next_room is None:
                print("You can't go that way")
            else:
                current_room = next_room

        elif user_input.lower() == "west" or user_input.lower() == "w":
            next_room = room_list[current_room][4]
            if next_room is None:
                print("You can't go that way")
            else:
                current_room = next_room

        elif user_input.lower() == "quit":
            done = True

main()
