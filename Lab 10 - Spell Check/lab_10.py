"""read in dictionary as a loop
(read into a list)
read in the story - each item is a line
loop for each line
    word_list split line into words
    code is given -
    loop for each word in word_list (not line)
        run linear search. key is word in word_list
        complete search is here
"""


import re


# This function takes in a line of text and returns
# a list of words in the line.
def split_line(line):
    return re.findall('[A-Za-z]+(?:\'[A-Za-z]+)?', line)

file = open("dictionary.txt")
dictionary_list = []
for line in file:
    line = line.strip()
    dictionary_list.append(line)

file.close()

print("Linear Search")

file = open("AliceInWonderLand200.txt")
line_number = 0
for line in file:
    words = split_line(line)
    line_number += 1

    for word in words:
        i = 0
        while i < len(dictionary_list) and dictionary_list[i] != word.upper():
            i += 1

        if i >= len(dictionary_list):
            print("Line", line_number, "misspelled word:", word)

file.close()

print()
print("Binary Search")

file = open("dictionary.txt")
dictionary_list = []
for line in file:
    line = line.strip()
    dictionary_list.append(line)

file.close()


file = open("AliceInWonderLand200.txt")
line_number = 0
for line in file:

    words = split_line(line)
    line_number += 1

    for word in words:

        low = 0
        high = len(dictionary_list) - 1

        found = False
        while low <= high and not found:
            middle_pos = (low + high) // 2

            if dictionary_list[middle_pos] < word.upper():
                low = middle_pos + 1
            elif dictionary_list[middle_pos] > word.upper():
                high = middle_pos - 1
            else:
                found = True

        if not found:
            print("line", line_number, "misspelled word:", word)


file.close()
