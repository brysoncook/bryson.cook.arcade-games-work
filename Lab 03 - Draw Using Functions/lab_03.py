"""import the arcade library"""
import arcade

def draw_fish(position_x, position_y):
    # Draw body of fish
    arcade.draw_ellipse_filled(position_x, position_y, 20, 10, arcade.color.CARIBBEAN_GREEN)

    # Draw tail of fish
    arcade.draw_triangle_filled(position_x - 10, position_y - 1,
                                position_x - 25, position_y - 10,
                                position_x - 25, position_y + 10,
                                arcade.color.CORAL_RED)

    # draw eye on fish
    arcade.draw_circle_outline(position_x + 10, position_y + 2, 2, arcade.color.BLACK)


def draw_palm_tree(position_x, position_y):
    arcade.draw_rectangle_filled(position_x, position_y + 2, 4, 20, arcade.color.COCONUT)
    arcade.draw_ellipse_filled(position_x + 5, position_y + 15, 10, 5, arcade.color.AO, 35)
    arcade.draw_ellipse_filled(position_x - 5, position_y + 15, 10, 5, arcade.color.AO, 145)
    arcade.draw_ellipse_filled(position_x + 5, position_y + 10, 10, 5, arcade.color.AO, 335)
    arcade.draw_ellipse_filled(position_x - 5, position_y + 10, 10, 5, arcade.color.AO, 205)

def draw_cloud(position_x, position_y):
    arcade.draw_circle_filled(position_x, position_y - 10, 20, arcade.color.AZURE_MIST)
    arcade.draw_circle_filled(position_x + 15, position_y, 20, arcade.color.AZURE_MIST)
    arcade.draw_circle_filled(position_x, position_y + 10, 20, arcade.color.AZURE_MIST)
    arcade.draw_circle_filled(position_x - 15, position_y, 20, arcade.color.AZURE_MIST)

# Side of the screen
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 400

# Size of the rectangle
ELLIPSE_WIDTH = 40
ELLIPSE_HEIGHT = 15

def on_draw(delta_time):
    """
    Use this function to draw everything to the screen.
    """

    # Start the render. This must happen before any drawing
    # commands. We do NOT need a stop render command.
    arcade.start_render()

    # Draw an ellipse.
    arcade.draw_ellipse_filled(on_draw.center_x, on_draw.center_y,
                                 ELLIPSE_WIDTH, ELLIPSE_HEIGHT,
                                 arcade.color.ALIZARIN_CRIMSON)

    # Modify ellipse's position based on the delta
    # vector. (Delta means change. You can also think
    # of this as our speed and direction.)
    on_draw.center_x += on_draw.delta_x * delta_time
    on_draw.center_y += on_draw.delta_y * delta_time

    # Figure out if we hit the edge and need to reverse.
    if on_draw.center_x < ELLIPSE_WIDTH // 2 \
            or on_draw.center_x > SCREEN_WIDTH - ELLIPSE_WIDTH // 2:
        on_draw.delta_x *= -1
    if on_draw.center_y < ELLIPSE_HEIGHT // 2 \
            or on_draw.center_y > (SCREEN_HEIGHT / 2) - ELLIPSE_HEIGHT // 2:
        on_draw.delta_y *= -1

    # Set sky color
    arcade.draw_lrtb_rectangle_filled(0, 600, 400, 200, arcade.color.AIR_SUPERIORITY_BLUE)

    # Island
    arcade.draw_ellipse_filled(45, 200, 50, 10, arcade.color.BLOND)
    arcade.draw_rectangle_filled(45, 195, 100, 10, (19, 57, 212))

    # Head of person in boat
    arcade.draw_commands.draw_circle_filled(250, 180, 10, arcade.color.ALLOY_ORANGE)

    # Body of person in boat
    arcade.draw_ellipse_filled(250, 150, 20, 10, arcade.color.LIME_GREEN, 90)

    # Hand of person in boat
    arcade.draw_circle_filled(265, 160, 4, arcade.color.ALLOY_ORANGE)

    # Fishing pool for person in boat
    arcade.draw_line(265, 160, 360, 190, arcade.color.BLACK_BEAN, 3)

    # Fishing line on fishing pool
    arcade.draw_line(360, 190, 380, 100, arcade.color.BLOND)

    # Base of the boat in brown (right, left, up, down, thickness, length)
    arcade.draw_arc_filled(200, 150, 60, 100, arcade.color.BROWN_NOSE, 90, 270, 90)

    # Mast for the boat
    arcade.draw_line(200, 270, 200, 150, arcade.color.SMOKY_BLACK, 8)

    # Sail on mast
    arcade.draw_triangle_filled(100, 155, 192, 270, 192, 155, arcade.color.ANTIQUE_WHITE)

    # Sun in the sky
    arcade.draw_circle_filled(450, 330, 35, (255, 217, 0))

    # Water coming in front of boat
    arcade.draw_rectangle_filled(200, 100, 150, 20, (19, 57, 212))

    draw_palm_tree(75, 210, )
    draw_palm_tree(20, 210, )
    draw_palm_tree(45, 215, )

    draw_fish(100, 50, )
    draw_fish(30, 100, )
    draw_fish(310, 60, )
    draw_fish(420, 130, )

    draw_cloud(70, 300)
    draw_cloud(150, 345)
    draw_cloud(320, 250)
    draw_cloud(500, 235)



# Below are function-specific variables. Before we use them
# in our function, we need to give them initial values. Then
# the values will persist between function calls.
#
# In other languages, we'd declare the variables as 'static' inside the
# function to get that same functionality.
#
# Later on, we'll use 'classes' to track position and velocity for multiple
# objects.
on_draw.center_x = 100      # Initial x position
on_draw.center_y = 50       # Initial y position
on_draw.delta_x = 50  # Initial change in x
on_draw.delta_y = 60  # Initial change in y

def main():

    # open up a window.
    arcade.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, "Drawing Window")
    arcade.set_background_color((19,57,212))
    # calls the draw command on the specified interval.
    arcade.schedule(on_draw, 1 / 60)

    # Keep the window up until someone closes it.
    arcade.run()
if __name__ == "__main__":
    main()
