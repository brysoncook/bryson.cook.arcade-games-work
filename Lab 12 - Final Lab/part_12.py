
import arcade
import random
# Images and audio from Kenney.nl


SPRITE_SCALING = 0.5
ENEMY_SCALING = 1.9

SCREEN_WIDTH = 1000
SCREEN_HEIGHT = 600

BULLET_SPEED = 8

INSTRUCTION_PAGE = 0
GAME_RUNNING = 1
GAME_OVER = 2


class Bullet(arcade.Sprite):

    def update(self):
        self.center_y += BULLET_SPEED


class EnemySlow(arcade.Sprite):

    def reset_pos(self):
        self.center_y = random.randrange(SCREEN_HEIGHT + 20,
                                         SCREEN_HEIGHT + 300)
        self.center_x = random.randrange(SCREEN_WIDTH)

    def update(self):
        self.center_y -= random.randrange(1, 5)

        if self.top < 0:
            self.reset_pos()


class EnemyFast(arcade.Sprite):

    def reset_pos(self):
        self.center_y = random.randrange(SCREEN_HEIGHT + 20,
                                         SCREEN_HEIGHT + 300)
        self.center_x = random.randrange(SCREEN_WIDTH)

    def update(self):
        self.center_y -= random.randrange(3, 8)

        if self.top < 0:
            self.reset_pos()


class SpaceShip(arcade.Sprite):

    def __init__(self, filename, scale):

        super().__init__(filename, scale)
        self.speed = 0

    def update(self):

        self.center_x = 50
        self.center_y = 70

        super().update()


class MyWindow(arcade.Window):
    def __init__(self):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "sprites and bullets")

        arcade.set_background_color(arcade.color.BRIGHT_MAROON)

        self.current_state = INSTRUCTION_PAGE

        self.all_sprites_list = None
        self.enemy_list = None
        self.bullet_list = None
        self.ship_life_list = None

        self.level = 1
        self.score = 0
        self.player_sprite = None
        self.lives = 3

        # Audio for level up https://freesound.org/people/InfiniteLifespan/sounds/266454/
        self.level_up_sound = arcade.load_sound("sounds/level_up.wav")
        self.laser_sound = arcade.load_sound("sounds/laser2.ogg")
        self.hit_enemy_sound = arcade.load_sound("sounds/hit_enemy.2.ogg")
        self.hit_wall_sound = arcade.load_sound("sounds/hit_enemy.ogg")
        self.lose_life_sound = arcade.load_sound("sounds/lose_life.ogg")
        self.game_over_sound = arcade.load_sound("sounds/gameover.ogg")

        # image from www.sarmy.org.au/Global/SArmy/Resources/powerpoints/salvo/galaxies/16x9/Galaxies-01.jpg
        self.instruction = []
        texture = arcade.load_texture("images/Untitled.jpg")
        self.instruction.append(texture)

    def setup(self):

        self.all_sprites_list = arcade.SpriteList()
        self.bullet_list = arcade.SpriteList()
        self.enemy_list = arcade.SpriteList()
        self.ship_life_list = arcade.SpriteList()

        self.level = 1

        self.level_1()
        self.score = 0
        self.player_sprite = arcade.Sprite("images/playerShip3_green.png", SPRITE_SCALING)
        self.all_sprites_list.append(self.player_sprite)
        self.lives = 3

        self.player_sprite.center_x = SCREEN_WIDTH / 2
        self.player_sprite.center_y = 50

        self.set_mouse_visible(False)

    def draw_instruction_page(self, page_number):

        page_texture = self.instruction[page_number]
        arcade.draw_texture_rectangle(SCREEN_WIDTH // 2, SCREEN_HEIGHT // 2, page_texture.width,
                                      page_texture.height, page_texture, 0)

    def level_1(self):
        for i in range(30):

            enemy_sprite = arcade.Sprite("images/meteorBrown_big3.png", SPRITE_SCALING)

            enemy_sprite.center_x = random.randrange(SCREEN_WIDTH)
            enemy_sprite.center_y = random.randrange(100, 600)

            self.all_sprites_list.append(enemy_sprite)
            self.enemy_list.append(enemy_sprite)

    def level_2(self):
        image_list = ("images/meteorBrown_med3.png",
                      "images/meteorBrown_big3.png")
        for i in range(30):
            image_no = random.randrange(2)
            enemy_sprite = EnemySlow(image_list[image_no], ENEMY_SCALING / 4)

            enemy_sprite.center_x = random.randrange(SCREEN_WIDTH)
            enemy_sprite.center_y = random.randrange(SCREEN_HEIGHT)

            self.all_sprites_list.append(enemy_sprite)
            self.enemy_list.append(enemy_sprite)

    def level_3(self):
        image_list = ("images/meteorBrown_small2.png",
                      "images/meteorBrown_med3.png",
                      "images/meteorBrown_big3.png")
        for i in range(30):
            image_no = random.randrange(3)
            enemy_sprite = EnemyFast(image_list[image_no], ENEMY_SCALING / 4)

            enemy_sprite.center_x = random.randrange(SCREEN_WIDTH)
            enemy_sprite.center_y = random.randrange(SCREEN_HEIGHT)

            self.all_sprites_list.append(enemy_sprite)
            self.enemy_list.append(enemy_sprite)

    def draw_game(self):

        arcade.start_render()

        self.all_sprites_list.draw()

        output = "Lives: " + str(self.lives)
        arcade.draw_text(output, 870, 560, arcade.color.WHITE, 15)

        output = "Score: " + str(self.score)
        arcade.draw_text(output, 870, 580, arcade.color.WHITE, 15)

        output = "Level: " + str(self.level)
        arcade.draw_text(output, 10, 580, arcade.color.WHITE, 15)

    def draw_game_over(self):
        output = "Game Over"
        arcade.draw_text(output, SCREEN_WIDTH / 2 - 150, SCREEN_HEIGHT / 2 + 50, arcade.color.GOLDENROD, 50)

        output = "Click to restart"
        arcade.draw_text(output, SCREEN_WIDTH / 2 - 100, SCREEN_HEIGHT / 2, arcade.color.WHITE, 24)

    def on_draw(self):

        arcade.start_render()

        if self.current_state == INSTRUCTION_PAGE:
            self.draw_instruction_page(0)

        elif self.current_state == GAME_RUNNING:
            self.draw_game()

        else:
            self.draw_game()
            self.draw_game_over()

    def on_key_press(self, key, modifiers):

        if key == arcade.key.SPACE:
            arcade.play_sound(self.laser_sound)
            bullet = Bullet("images/laserGreen11.png", SPRITE_SCALING * 1.5)
            bullet.angle = 0

            bullet.center_x = self.player_sprite.center_x
            bullet.bottom = self.player_sprite.top

            self.all_sprites_list.append(bullet)
            self.bullet_list.append(bullet)

        if key == arcade.key.LEFT:
            self.player_sprite.change_x = -5

        if key == arcade.key.RIGHT:
            self.player_sprite.change_x = 5

    def on_key_release(self, key, modifiers):
        if key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.player_sprite.change_x = 0

    def on_mouse_press(self, x, y, button, modifiers):

        if self.current_state == INSTRUCTION_PAGE:
            self.setup()
            self.current_state = GAME_RUNNING
        elif self.current_state == GAME_OVER:
            self.setup()
            self.current_state = INSTRUCTION_PAGE

    def update(self, delta_time):

        if self.current_state == GAME_RUNNING:
            self.all_sprites_list.update()
            if self.player_sprite.left < 0:
                self.player_sprite.left = 0
                arcade.play_sound(self.hit_wall_sound)
            if self.player_sprite.right > SCREEN_WIDTH:
                self.player_sprite.right = SCREEN_WIDTH
                arcade.play_sound(self.hit_wall_sound)
            for bullet in self.bullet_list:
                enemies = \
                    arcade.check_for_collision_with_list(bullet, self.enemy_list)

                if self.level == 3:
                    for enemy in enemies:
                        arcade.play_sound(self.hit_enemy_sound)
                        self.score += 100
                        bullet.kill()

                        enemy.center_y = random.randrange(SCREEN_HEIGHT + 20,
                                                          SCREEN_HEIGHT + 100)
                        enemy.center_x = random.randrange(SCREEN_WIDTH)
                else:
                    for enemy in enemies:
                        arcade.play_sound(self.hit_enemy_sound)
                        enemy.kill()
                        self.score += 100
                        bullet.kill()

                if bullet.bottom > SCREEN_HEIGHT:
                    bullet.kill()

            enemies = \
                arcade.check_for_collision_with_list(self.player_sprite,
                                                     self.enemy_list)
            if len(enemies) > 0:
                if self.lives > 0:
                    arcade.play_sound(self.lose_life_sound)
                    self.lives -= 1
                    enemies[0].kill()
                else:
                    self.current_state = GAME_OVER
                    arcade.play_sound(self.game_over_sound)

            if self.score >= 2000 and self.level == 1:
                arcade.play_sound(self.level_up_sound)
                self.level += 1
                self.level_2()
            elif self.score >= 5000 and self.level == 2:
                arcade.play_sound(self.level_up_sound)
                self.level += 1
                self.level_3()
            elif len(self.enemy_list) == 0 and self.level == 3:
                self.current_state = GAME_OVER


def main():
    MyWindow()
    arcade.run()


if __name__ == "__main__":
    main()
