import arcade

# --- Constants ---
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
ERROR_SOUND = arcade.load_sound("error3.ogg")


class MyFish:
    def __init__(self, position_x, position_y):
        self.position_x = position_x
        self.position_y = position_y
        self.error_sound = arcade.load_sound("error3.ogg")

    def draw(self):
        # Draw body of fish
        arcade.draw_ellipse_filled(self.position_x, self.position_y, 20, 10, arcade.color.CARIBBEAN_GREEN)

        # Draw tail of fish
        arcade.draw_triangle_filled(self.position_x - 10, self.position_y - 1,
                                    self.position_x - 25, self.position_y - 10,
                                    self.position_x - 25, self.position_y + 10,
                                    arcade.color.CORAL_RED)

        # draw eye on fish
        arcade.draw_circle_outline(self.position_x + 10, self.position_y + 2, 2, arcade.color.BLACK)

    def update(self):
        if self.position_x < self.position_x + 20:
            self.position_x = self.position_x + 20

        if self.position_x > SCREEN_WIDTH - self.position_x + 20:
            self.position_x = SCREEN_WIDTH - self.position_x + 20

        if self.position_y < self.position_y + 20:
            self.position_y = self.position_y + 20

        if self.position_y > SCREEN_HEIGHT - self.position_y + 20:
            self.position_y = SCREEN_HEIGHT - self.position_y + 20


class Cloud:
    def __init__(self, position_x, position_y, change_x, change_y, radius):
        self.position_x = position_x
        self.position_y = position_y
        self.change_x = change_x
        self.change_y = change_y
        self.radius = radius
        self.error_sound = arcade.load_sound("error3.ogg")

    def draw(self):
        arcade.draw_circle_filled(self.position_x, self.position_y - 10, 20, arcade.color.AZURE_MIST)
        arcade.draw_circle_filled(self.position_x + 15, self.position_y, 20, arcade.color.AZURE_MIST)
        arcade.draw_circle_filled(self.position_x, self.position_y + 10, 20, arcade.color.AZURE_MIST)
        arcade.draw_circle_filled(self.position_x - 15, self.position_y, 20, arcade.color.AZURE_MIST)

    def update(self):
        self.position_y += self.change_y
        self.position_x += self.change_x

        # See if the ball hit the edge of the screen. If so, change direction
        if self.position_x < self.radius:
            self.position_x = self.radius

        if self.position_x > SCREEN_WIDTH - self.radius:
            self.position_x = SCREEN_WIDTH - self.radius

        if self.position_y < self.radius:
            self.position_y = self.radius

        if self.position_y > SCREEN_HEIGHT - self.radius:
            self.position_y = SCREEN_HEIGHT - self.radius

        if self.position_x >= SCREEN_WIDTH - 40 or self.position_x <= 40:
            arcade.play_sound(self.error_sound)
        elif self.position_y <= 40 or self.position_y >= SCREEN_HEIGHT - 40:
            arcade.play_sound(self.error_sound)


class MyWindow(arcade.Window):
    """ Our Custom Window Class"""

    def __init__(self):
        """ Initializer """

        # Call the parent class initializer
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 7 - User Control")

        self.cloud = Cloud(200, 420, 0, 0, 35)

        self.set_mouse_visible(False)
        self.fish = MyFish(400, 230)

        self.error_sound = arcade.load_sound("error3.ogg")

        arcade.set_background_color(arcade.color.AZURE)

    def on_draw(self):
        arcade.start_render()

        def draw_fish(position_x, position_y):
            # Draw body of fish
            arcade.draw_ellipse_filled(position_x, position_y, 20, 10, arcade.color.CARIBBEAN_GREEN)

            # Draw tail of fish
            arcade.draw_triangle_filled(position_x - 10, position_y - 1,
                                        position_x - 25, position_y - 10,
                                        position_x - 25, position_y + 10,
                                        arcade.color.CORAL_RED)

            # draw eye on fish
            arcade.draw_circle_outline(position_x + 10, position_y + 2, 2, arcade.color.BLACK)

        def draw_cloud(position_x, position_y):
            arcade.draw_circle_filled(position_x, position_y - 10, 20, arcade.color.AZURE_MIST)
            arcade.draw_circle_filled(position_x + 15, position_y, 20, arcade.color.AZURE_MIST)
            arcade.draw_circle_filled(position_x, position_y + 10, 20, arcade.color.AZURE_MIST)
            arcade.draw_circle_filled(position_x - 15, position_y, 20, arcade.color.AZURE_MIST)

        arcade.draw_rectangle_filled(400, 100, 800, 300, (19, 57, 212))

        # Head of person in boat
        arcade.draw_commands.draw_circle_filled(250, 180, 10, arcade.color.ALLOY_ORANGE)

        # Body of person in boat
        arcade.draw_ellipse_filled(250, 150, 20, 10, arcade.color.LIME_GREEN, 90)

        # Hand of person in boat
        arcade.draw_circle_filled(265, 160, 4, arcade.color.ALLOY_ORANGE)

        # Fishing pool for person in boat
        arcade.draw_line(265, 160, 360, 190, arcade.color.BLACK_BEAN, 3)

        # Fishing line on fishing pool
        arcade.draw_line(360, 190, 380, 100, arcade.color.BLOND)

        # Base of the boat in brown (right, left, up, down, thickness, length)
        arcade.draw_arc_filled(200, 150, 60, 100, arcade.color.BROWN_NOSE, 90, 270, 90)

        # Mast for the boat
        arcade.draw_line(200, 270, 200, 150, arcade.color.SMOKY_BLACK, 8)

        # Sail on mast
        arcade.draw_triangle_filled(100, 155, 192, 270, 192, 155, arcade.color.ANTIQUE_WHITE)

        # Sun in the sky
        arcade.draw_circle_filled(700, 500, 50, (255, 217, 0))

        # Water coming in front of boat
        arcade.draw_rectangle_filled(200, 100, 150, 20, (19, 57, 212))

        draw_fish(100, 50)
        draw_fish(30, 100)
        draw_fish(310, 60)
        draw_fish(420, 130)
        draw_fish(600, 170)
        draw_fish(670, 120)

        draw_cloud(170, 400)
        draw_cloud(250, 545)
        draw_cloud(420, 450)
        draw_cloud(600, 335)

        self.fish.draw()
        self.cloud.draw()

    def on_mouse_motion(self, x, y, dx, dy):
        self.fish.position_x = x
        self.fish.position_y = y

        if x <= 10 or x >= SCREEN_WIDTH - 10:
            arcade.play_sound(self.error_sound)
        elif y <= 10 or y >= SCREEN_HEIGHT - 10:
            arcade.play_sound(self.error_sound)

    def update(self, delta_time):
        self.cloud.update()

    def on_key_press(self, key, modifiers):
        if key == arcade.key.LEFT:
            self.cloud.change_x = -3
        elif key == arcade.key.RIGHT:
            self.cloud.change_x = 3
        elif key == arcade.key.UP:
            self.cloud.change_y = 3
        elif key == arcade.key.DOWN:
            self.cloud.change_y = -3

    def on_mouse_press(self, x, y, button, modifiers):
        if button == arcade.MOUSE_BUTTON_LEFT:
            arcade.play_sound(self.error_sound)

    def on_key_release(self, key, modifiers):
        """ Called whenever a user releases a key. """
        if key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.cloud.change_x = 0
        elif key == arcade.key.UP or key == arcade.key.DOWN:
            self.cloud.change_y = 0


def main():
    window = MyWindow()
    arcade.run()

main()