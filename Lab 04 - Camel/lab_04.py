import random

def main():
    print("Welcome to Camel!")
    print("""You have stolen a camel to make your way across the great
Mobi desert. the natives want their camel back and are chasing
you down! survive your desert trek and out run the natives.\n""")

    miles_traveled = 0
    thirst = 0
    camel_tiredness = 0
    oasis = 0
    canteen = 3
    natives_traveled = -20
    done = False

    while not done:
        print("""
A. Drink form your canteen.
B. Ahead moderate speed.
C. Ahead full speed.
D. Stop for the night.
E. Status check.
Q. Quit.\n """)
        user_choice = input("What is your choice? ").upper()
        print()
        if user_choice == "Q":
            done = True

        elif user_choice == "E":
            natives_behind = miles_traveled - natives_traveled
            print("Miles traveled: ", miles_traveled)
            print("Drinks in canteen: ", canteen)
            print("Your camel has", camel_tiredness ,"amount of fatigue")
            print("You have", thirst ,"amount of thirst")
            print("The natives are", natives_behind ,"miles behind you.")

        elif user_choice == "D":
            camel_tiredness = 0
            print("You have decided to stop for the night.")
            print("The camel is happy.")
            natives_traveled += random.randrange(7, 15)

        elif user_choice == "C":
            full_speed = random.randrange(10, 21)
            print("You have traveled", full_speed, "miles.")
            miles_traveled += full_speed
            natives_traveled += random.randrange(7, 15)
            thirst += 1
            camel_tiredness += random.randrange(1, 4)
            oasis = random.randrange(1, 21)

        elif user_choice == "B":
            moderate_speed = random.randrange(5, 13)
            print("You have traveled", moderate_speed, "miles")
            miles_traveled = miles_traveled + moderate_speed
            natives_traveled += random.randrange(7, 15)
            thirst += 1
            camel_tiredness += 1
            oasis = random.randrange(1, 21)

        elif user_choice == "A":
            if canteen == 0:
                print("You are out of water.")
            else:
                canteen -= 1
                thirst = 0
                print("You have taken a drink for your canteen, you are no longer thirsty.")
        else:
            print("Invalid input; Try again.")

        if miles_traveled >= 200 and not done:
            print("Congratulations you have won the game and escaped from the natives!")
            done = True
        if thirst > 6 and not done:
            print("You died of thirst!")
            done = True
        if camel_tiredness > 8 and not done:
            print("Your camel has died.")
            done = True
        if oasis == 20 and not done:
            canteen = 0
            camel_tiredness = 0
            canteen = 3
            print("You found an oasis! After taking a drink you filled your canteen and the camel is refreshed.")
        if natives_traveled >= miles_traveled and not done:
            print("The natives have cought you and taken their camel back")
            done = True
        if thirst > 4 <= 6 and not done:
            print("You are thirsty.")
        if camel_tiredness > 5 and camel_tiredness <= 8 and not done:
            print("Your camel is getting tired.")
        if miles_traveled - natives_traveled <= 15 and not done:
            print("The natives are getting close!")

main()




