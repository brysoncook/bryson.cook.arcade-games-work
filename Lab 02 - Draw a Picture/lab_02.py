""" Lab two drawing with programing
This image will be of a boat in the ocean
with a island in the background."""

"""import the arcade library"""
import arcade

# open up a window.
arcade.open_window(600,400,"Drawing Window")

# Set background color of window
arcade.set_background_color((19,57,212))

# Get ready to draw
arcade.start_render()

# Set sky color
arcade.draw_lrtb_rectangle_filled(0, 600, 400, 200, arcade.color.AIR_SUPERIORITY_BLUE)

# Island
arcade.draw_ellipse_filled(45, 200, 50, 10, arcade.color.BLOND)

# Trunk of left palm tree on island
arcade.draw_line(25, 204, 25, 226, arcade.color.COCONUT, 5)

# Palm leaves on left tree
arcade.draw_ellipse_filled(18, 229, 10, 5, arcade.color.AO, 145)

# Palm leaves on left tree
arcade.draw_ellipse_filled(32, 229, 10, 5, arcade.color.AO, 35)

# Palm leaves on left tree
arcade.draw_ellipse_filled(33, 223, 10, 5, arcade.color.AO, 335)

# Palm leaves on left tree
arcade.draw_ellipse_filled(17, 223, 10, 5, arcade.color.AO, 205)

# Trunk of right palm tree on island
arcade.draw_line(60, 204, 60, 226, arcade.color.COCONUT, 5)

# Palm leaves on right tree
arcade.draw_ellipse_filled(53, 229, 10, 5, arcade.color.AO, 145)

# Palm leaves on right tree
arcade.draw_ellipse_filled(67, 229, 10, 5, arcade.color.AO, 35)

# Palm leaves on right tree
arcade.draw_ellipse_filled(68, 223, 10, 5, arcade.color.AO, 335)

# Palm leaves on right tree
arcade.draw_ellipse_filled(52, 223, 10, 5, arcade.color.AO, 205)

# Water in from of Island
arcade.draw_rectangle_filled(45, 190, 100, 20, (19,57,212))

# Head of person in boat
arcade.draw_commands.draw_circle_filled(250, 180, 10, arcade.color.ALLOY_ORANGE)

# Body of person in boat
arcade.draw_ellipse_filled(250, 150, 20, 10, arcade.color.LIME_GREEN, 90)

# Hand of person in boat
arcade.draw_circle_filled(265, 160, 4, arcade.color.ALLOY_ORANGE)

# Fishing pool for person in boat
arcade.draw_line(265, 160, 360, 190, arcade.color.BLACK_BEAN, 3)

# Fishing line on fishing pool
arcade.draw_line(360, 190, 380, 100, arcade.color.BLOND)

# Base of the boat in brown (right, left, up, down, thickness, length)
arcade.draw_arc_filled(200, 150, 60, 100, arcade.color.BROWN_NOSE, 90, 270, 90)

# Mast for the boat
arcade.draw_line(200, 270, 200, 150, arcade.color.SMOKY_BLACK, 8)

# Sail on mast
arcade.draw_triangle_filled(100, 155, 192, 270, 192, 155, arcade.color.ANTIQUE_WHITE)

# Sun in the sky
arcade.draw_circle_filled(450, 330, 35, (255, 217, 0))

# Water coming in front of boat
arcade.draw_rectangle_filled(200, 85, 200, 50, (19, 57, 212))

# Finish drawing
arcade.finish_render()

# Keep the window up until someone closes it.
arcade.run()
