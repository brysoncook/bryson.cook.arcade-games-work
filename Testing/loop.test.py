
# loop for certain number of times
# range can have 3 parameters
for i in range(10, -1, -1):
    print(i)

sum = 0
for n in range(1, 101):
    sum = sum + n
print(sum)

a = 0
for i in range(10):
    a = a + 1
    for j in range(10):
        a = a + 1
print(a)

i = 0
while i < 10:
    print(i)
    i += 1

# done = False
# while not done:
#     quit = input("Do you want to quit? ")
#     if quit == "y":
#         done = True
#     if not done:
#         attack = input("Does your elf attack the dragon? ")
#         if attack == "y":
#             print("Bad choice, you died.")
#             done = True

for i in range(10):
    print("*", end=" ")
print()
for i in range(5):
    print("*", end=" ")
print()
for i in range(20):
    print("*", end=" ")
print()

for row in range(10):
    for column in range(10):
        print("*",end=" ")
    print()

for row in range(10):
    for column in range(5):
        print("*",end=" ")
    print()

for row in range(5):
    for column in range(20):
        print("*",end=" ")
    print()

for j in range (10):
    for i in range(10):
        print(i,end=" ")
        i += i
    print()

print()

for row in range(10):
    for column in range(10):
        print(column,end=" ")
    print()

print()

for column in range(10):
    for row in range(10):
        print(column, end=" ")
    print()

print()

for row in range(10):
    for column in range(row+1):
        print(column,end=" ")
    print()

print()

for row in range(10):
    for j in range(row):
        print("",end=" ")
    for column in range(column):
        print(column,end=" ")
    print()
