# class Character():
#     """method is a function in a class"""
#     def __init__(self):
#         """Set up my class."""
#         self.name = "Link"
#         self.sex = "Male"
#         self.max_hit_points = 50
#         self.current_hit_points = 50
#
# class Address():
#     def __init__(self):
#         self.name = ""
#         self.line1 = ""
#         self.line2 = ""
#         self.city = ""
#         self.state = ""
#         self.zip_code = ""
# class Dog():
#     def __init__(self):
#         self.age = 0
#         self.name = ""
#         self.weight = 0
#     def bark(self):
#         print("Woof says", self.name)
# import arcade
#
# class Ball():
#     def __init__(self):
#         self.color = arcade.color.WHITE
#         self.x = 0
#         self.y = 0
#         self.change_x = 0
#         self.change_y = 0
#     def move(self):
#         self.x += self.change_x
#         self.y += self.change_y
#
#
#
# class Bank():
#     def __init__(self):
#         self.account_number = ""
#         self.balance = 0
#
#     def deposit(self, amount):
#         self.balance += amount
#     def withdraw(self, amount):
#         self.balance -= amount
#
# class Person():
#     def __init__(self):
#         self.name = ""
#         self.money = 0
# # doesnt work, doesnt change the amount of money
# def give_money1(money):
#     money += 100
# # works to give dogs money
# def give_money2(person):
#     person.money += 100
#
# class Dog():
#     def __init__(self, new_name):
#         self.name = new_name
#
#
# class Boat():
#     def __init__(self):
#         self.tonnage = 0
#         self.name = ""
#         self.is_docked = True
#
#     def dock(self):
#         if self.is_docked:
#             print("You are already docked.")
#         else:
#             self.is_docked = True
#             print("Docking")
#
#     def undock(self):
#         if not self.is_docked:
#             print("You aren't docked.")
#         else:
#             self.is_docked = False
#             print("Undocking")
#
# class Submarine(Boat):
#     def submerge(self):
#         print("Submerge!")
#
#
#
# def main():
#     my_dog = Dog("Spot")
#     print("Your dogs name is:", my_dog.name)
#
#     my_name1 = Person()
#     my_name1.name = "Bob"
#     my_name1.money = 100
#     print(my_name1.name, "$", my_name1.money, )
#
#     give_money2(my_name1)
#     print(my_name1.name, "$",my_name1.money,)
#
#     my_name2 = Person()
#     my_name2.name = "nancy"
#     print(my_name2.name, "$",my_name2.money)
#
#     my_account = Bank()
#     my_account.account_number = "1234321"
#     my_account.deposit(100)
#     my_account.deposit(420)
#     my_account.withdraw(100)
#     print(my_account.balance)
#
#     my_ball = Ball()
#     my_ball.x = 100
#     my_ball.y = 100
#     my_ball.change_x = 2
#     my_ball.change_y = -1
#     for i in range(100):
#         print(my_ball.x, my_ball.y)
#         my_ball.move()
#
#
#     # my_dog = Dog()
#     # my_dog.name = "Spot"
#     # my_dog.bark()
#
#
#
#
#
#     home_address = Address()
#     home_address.name = "Jane Smith"
#     home_address.line1 = "701 N. C. Street"
#     home_address.line2 = "carver Science Building"
#     home_address.city = "Indianola"
#     home_address.state = "IA"
#     home_address.zip_code = "50125"
#
#
# main()

# import arcade
# import random
#
# SCREEN_WIDTH = 640
# SCREEN_HEIGHT = 480
#
#
# class Ball:
#     def __init__(self, position_x, position_y, change_x, change_y, radius, color):
#
#         # Take the parameters of the init function above, and create instance variables out of them.
#         self.position_x = position_x
#         self.position_y = position_y
#         self.change_x = change_x
#         self.change_y = change_y
#         self.radius = radius
#         self.color = color
#
#     def draw(self):
#         """ Draw the balls with the instance variables we have. """
#         arcade.draw_circle_filled(self.position_x, self.position_y, self.radius, self.color)
#
#     def update(self):
#         # Move the ball
#         self.position_y += self.change_y
#         self.position_x += self.change_x
#
#         # See if the ball hit the edge of the screen. If so, change direction
#         if self.position_x < self.radius:
#             self.change_x *= -1
#
#         if self.position_x > SCREEN_WIDTH - self.radius:
#             self.change_x *= -1
#
#         if self.position_y < self.radius:
#             self.change_y *= -1
#
#         if self.position_y > SCREEN_HEIGHT - self.radius:
#             self.change_y *= -1
#
#
# class MyApplication(arcade.Window):
#
#     def __init__(self, width, height, title):
#
#         # Call the parent class's init function
#         super().__init__(width, height, title)
#         arcade.set_background_color(arcade.color.ASH_GREY)
#
#         # Create a list for the balls
#         self.ball_list = []
#
#         # Add three balls to the list
#         ball = Ball(50, 50, 3, 3, 15, arcade.color.AUBURN)
#         self.ball_list.append(ball)
#
#         ball = Ball(100, 150, 2, 3, 15, arcade.color.PURPLE_MOUNTAIN_MAJESTY)
#         self.ball_list.append(ball)
#
#         ball = Ball(150, 250, -3, -1, 15, arcade.color.FOREST_GREEN)
#         self.ball_list.append(ball)
#
#     def on_draw(self):
#         """ Called whenever we need to draw the window. """
#         arcade.start_render()
#
#         # Use a "for" loop to pull each ball from the list, then call the draw
#         # method on that ball.
#         for ball in self.ball_list:
#             ball.draw()
#
#     def update(self, delta_time):
#         """ Called to update our objects. Happens approximately 60 times per second."""
#
#         # Use a "for" loop to pull each ball from the list, then call the animate
#         # method on that ball.
#         for ball in self.ball_list:
#             ball.update()
# window = MyApplication(640, 480, "Drawing Example")
#
# arcade.run()

import arcade

SCREEN_WIDTH = 640
SCREEN_HEIGHT = 480


class Ball:
    def __init__(self, position_x, position_y, radius, color):

        # Take the parameters of the init function above, and create instance variables out of them.
        self.position_x = position_x
        self.position_y = position_y
        self.radius = radius
        self.color = color

    def draw(self):
        """ Draw the balls with the instance variables we have. """
        arcade.draw_circle_filled(self.position_x, self.position_y, self.radius, self.color)


class MyApplication(arcade.Window):

    def __init__(self, width, height, title):

        # Call the parent class's init function
        super().__init__(width, height, title)

        # Make the mouse disappear when it is over the window.
        # So we just see our object, not the pointer.
        self.set_mouse_visible(False)

        arcade.set_background_color(arcade.color.ASH_GREY)

        # Create our ball
        self.ball = Ball(50, 50, 15, arcade.color.AUBURN)

    def on_draw(self):
        """ Called whenever we need to draw the window. """
        arcade.start_render()
        self.ball.draw()

    def on_mouse_motion(self, x, y, dx, dy):
        """ Called to update our objects. Happens approximately 60 times per second."""
        self.ball.position_x = x
        self.ball.position_y = y
    def on_mouse_press(self, x, y, button, modifiers):
        if button == arcade. MOUSE_BUTTON_LEFT:
            print("Left mouse button pressed at", x, y)
        elif button == arcade.MOUSE_BUTTON_RIGHT:
            print("Right mouse button pressed at", x, y)



window = MyApplication(640, 480, "Mouse Example")

arcade.run()