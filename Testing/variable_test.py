# for i in range(1, 11):
#     print("Bryson")
# print("done")

# for i in range(20):
#     print("Red")
#     print("Gold")
# print("done")

# for i in range(2, 101, 2):
#     print(i)

# i = 10
# while i >= 0:
#     print(i)
#     i -= 1
# print("blast off")

# import random
# x = random.randrange(1, 11)
# print(x)
#
# my_number = random.random() * 9 + 1
# print(my_number)

import random

head_count = 0
tail_count = 0

for i in range(50):
    coin = random.randrange(2)
    if coin == 0:
        print("heads")
        head_count += 1
    else:
        print("tails")
        tail_count += 1
print("Heads: ", head_count)
print("Tails: ", tail_count)


x = "This is a sample string"
print("x[0]=", x[0])
print("x[1]=", x[1])
print("x[-1]=", x[-1])
# Access 0-5
print("x[:6]=", x[:6])
# Access 6-end
print("x[6:]=", x[6:])
# Access 6-8
print("x[6:9]=", x[6:9])

a = "Hi there"
print(len(a))

for character in "this is a test.":
    print(character)

# months = "JanFebMarAprMayJunJulAugSepOctNovDec"
# n = int(input("Enter a month number: "))
# my_month = months[(n-1)*3:n*3]
# print(my_month)

plain_text = "This is a test. ABC abc"

for c in plain_text:
    x = ord(c)
    x += 1
    character_2 = chr(x)
    print(character_2, end="")
print()
encrypted_text = "Uijt!jt!b!uftu/!BCD!bcd"
for c in encrypted_text:
    x = ord(c)
    x -= 1
    character_2 = chr(x)
    print(character_2, end="")

print()
import random

def sum_numbers(the_list):
    my_sum = 0
    for item in the_list:
        my_sum += item
    return my_sum
def create_numbers():
    my_list = []
    for i in range(100):
        x = random.randrange(1, 1001)
        my_list.append(x)
    return my_list

def main():
    my_list = create_numbers()
    print(my_list)
    sum_numbers(my_list)
    my_sum = sum_numbers(my_list)
    print(my_sum)

main()


