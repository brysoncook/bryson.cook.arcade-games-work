""" Chapter 4
 "if" statements
  (conditional Statements)"""
# click in a spot then hold alt and click in a different line to type in 2 places

a = False
b = True
if not a:
    print("If statement tripped")


a = input("What is your name? ")

if a == "Paul":
    print("You have a great name!")
elif a == "Fred":
    print("great.")
else:
    print("your name is ok i guess")
print("Done")

# both of these do the same thing
a = input("Enter a number for a: ")
a = float(a)

b = float(input("Enter a number for b: "))

user_input = input("enter a number for c: ")
c = float(user_input)



# < less than
# > greater than
# <= less than or equal
# >= grater than or equal
# == equal, asking if one is equal to other
# != not equal

if a < b and a < c:
    print("a is less than b and c.")

