def sum(x, y):
    c = x + y
    return c

result = sum(12, 30) + 4
print(result)

print(sum(34, 56))

def volume_cylinder(radius, height):
    pi = 3.141592653589
    volume = pi * radius ** 2 * height
    return volume
result = volume_cylinder(2, 6) * 6
print(result)

# Function that prints the result
def sum_print(a, b):
    result = a + b
    print(result)

# Function that returns the results
def sum_return(a, b):
    result = a + b
    return result

# This prints the sum of 4+4
sum_print(4, 4)

# This does not
sum_return(4, 4)

# This will not set x1 to the sum
# It actually gets a value of 'None'
x1 = sum_print(4, 4)

# This will
x2 = sum_return(4, 4)

# example 8
def a(x):
    x = x + 1
    return x
x = 3
x = a(x)
print(x)

# Example 9
def a(x,y):
    x = x + 1
    y = y + 1
    return x
    return y
x = 10
y = 20
z = a(x, y)
print(z)

# example 13
def a(my_data):
    print("function a, my_data = ", my_data)
    my_data = 20
    print("function a, my_data = ", my_data)
my_data = 10

print("global scope, my_data =", my_data)
a(my_data)
print("global scope, my_data =", my_data)

# example 14
def a(my_list):
    print("function a, list = ", my_list)
    my_list = [10, 20, 30]
    print("function a, list = ", my_list)

my_list = [5, 2, 4]

print("global scope, list =", my_list)
a(my_list)
print("global scope, list =", my_list)

""" Example 15
New concept!
Covered in more detail in a later chapter"""
def a(my_list):
    print("function a, list = ", my_list)
    my_list[0] = 1000
    print("function a, list = ", my_list)

my_list = [5, 2, 4]

print("global scope, list =", my_list)
a(my_list)
print("global scope, list =", my_list)

def g():
    my_value = 22

g()
print()

