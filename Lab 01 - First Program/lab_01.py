print("Hello")
print("There")
print("I want to print a double quote, \" for some reason.")
print("The file is stored\n in c:\\ new folder")
print("It was a dark and stormy night.")
print("Suddenly a shot rang out!")
print("This\nis\nmy\nsample.")
print("This is a lot of \nstuff to learn.")
print("Here are a lot of slashes, \\\\ and then some \"quotes.\"")
print("""Here are 
even more quotes, 
for some 
reason?""")
