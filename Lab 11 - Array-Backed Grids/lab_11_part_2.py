def check(grid):
    for person in range(len(grid)):
        column_total = 0
        for row in range(len(grid)):
            column_total += grid[row][person]

        row_total = 0
        for column in range(len(grid)):
            row_total += grid[person][column]

        if row_total == 1 and column_total == len(grid):
            print("The person in the", person, "position is a celebrity")

print("Test 1, Should show #2 is a celebrity.")
grid = [[1, 1, 1, 0],
        [0, 1, 1, 0],
        [0, 0, 1, 0],
        [1, 0, 1, 1]]

check(grid)

print("Test 2, Should show no one is a celebrity.")
grid = [[1, 1, 1, 0, 1],
        [0, 1, 1, 0, 1],
        [0, 0, 1, 0, 0],
        [1, 0, 0, 1, 1],
        [1, 0, 0, 1, 1]]

check(grid)

print("Test 3, Should show #2 is a celebrity.")
grid = [[1, 1, 1, 0, 1],
        [0, 1, 1, 0, 1],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 1],
        [1, 0, 1, 1, 1]]

check(grid)

print("Test 4, Should show no one is a celebrity.")
grid = [[1, 1, 1, 0, 1],
        [0, 1, 1, 0, 1],
        [1, 0, 1, 0, 0],
        [0, 0, 1, 0, 1],
        [1, 0, 1, 1, 1]]

check(grid)
